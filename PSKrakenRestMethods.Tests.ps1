$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path) -replace '\.Tests\.', '.'
. "$here\$sut"

Describe "Invoke-PSKrakenRestMethod" {  
    Context "Successful request" {
        Mock Invoke-RestMethod { return @{error=$null; result="success"} }

        $result = Invoke-PSKrakenRestMethod -Method Get -Body "testbody" -Uri "http://fake.url.com"

        It "Passthrough http method and body" {
            Assert-MockCalled Invoke-RestMethod -Exactly -Times 1 -ParameterFilter {
                $Method -eq "Get" -and $Body -eq "testbody" -and $Uri -eq "http://fake.url.com"
            }
        }

        It "Return response result" {
            $result | Should Be "success"
        }        
    }

    Context "When request invocation fails" {
        Mock Invoke-RestMethod { throw "bad error!" }

        It "Rethrow exception" {
            { Invoke-PSKrakenRestMethod -Method Get -Uri "http://fake.url.com" } | Should Throw "bad error!"
        }
    }

    Context "When request responses with error" {
        Mock Invoke-RestMethod { return @{error="failure"; result=$null} }

        It "Throw exception with the error message from the response" {
            { Invoke-PSKrakenRestMethod -Method Get -Uri "http://fake.url.com" } | Should Throw "failure"
        }
    }
}