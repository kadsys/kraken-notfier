. .\PSKrakenPublicMarketDataApiWrappers.ps1
. .\PSKrakenApiEnumTypes.ps1
. .\JsonSchemaHelper.ps1

Describe "Get-PSKrakenServerTime" {
    $url = "https://api.kraken.com/0/public/Time"

    Mock Invoke-PSKrakenRestMethod { return "success" }

    $result = Get-PSKrakenServerTime

    It "Request $url" {
        Assert-MockCalled Invoke-PSKrakenRestMethod -Exactly -Times 1 -ParameterFilter {
            $Uri -eq $url
        }
    }

    It "Return response result" {
        $result | Should Be "success"
    }
}

Describe "Get-PSKrakenAssets" {
    $url = "https://api.kraken.com/0/public/Assets"

    Context "With Asset parameter" {
        Mock Invoke-PSKrakenRestMethod { return (
                Get-Content .\ResponseGetAssets.json | 
                    ConvertFrom-Json | 
                    Select-Object XETH
            )}

        $result = Get-PSKrakenAssets -Asset "XETH"

        It "Request $url with asset parameter in the body" {
            Assert-MockCalled Invoke-PSKrakenRestMethod -Exactly -Times 1 -ParameterFilter {
                $Uri -eq $url `
                    -and $Body["asset"] -eq "XETH"
            }
        }

        It "Return information for XETH asset" {
            $result | Measure-Object | Select-Object -ExpandProperty Count | Should Be 1
            $asset = $result | Select-Object -First 1
            $asset | Should Not Be $null
            $asset.Name | Should Be "XETH"
            $asset.AClass | Should Be "currency"
            $asset.Altname | Should Be "ETH"
            $asset.Decimals | Should Be 10
            $asset.DisplayDecimals | Should Be 5
        }        
    }

    Context "Without asset parameter" {
        Mock Invoke-PSKrakenRestMethod {
            return (Get-Content .\ResponseGetAssets.json | ConvertFrom-Json) 
        }

        $assets = Get-PSKrakenAssets

        It "Request $url without any body parameter" {
            Assert-MockCalled Invoke-PSKrakenRestMethod -Exactly -Times 1 -ParameterFilter {
                $Uri -eq $url `
                    -and $Body.Count -eq 0
            }
        }

        It "Return an array of assets with properties like Name, Altname, AClass, Decimal and DisplayDecimal" {
            ($assets | Measure-Object | Select-Object -ExpandProperty Count) -gt 1 | Should Be $true
            $first = $assets | Select-Object -First 1
            $first | Should Not Be $null
            $first.Name | Should Be "DASH"
            $first.AClass | Should Be "currency"
            $first.Altname | Should Be "DASH"
            $first.Decimals | Should Be 10
            $first.DisplayDecimals | Should Be 5
        }    
    }
}

Describe "Get-PSKrakenTradableAssetPairs" {
    $url = "https://api.kraken.com/0/public/AssetPairs"
    
    Mock Invoke-PSKrakenRestMethod { return "success" }
    
    $result = Get-PSKrakenTradableAssetPairs -Info leverage -Pair "XETHZEUR"
    
    It "Request $url" {
        Assert-MockCalled Invoke-PSKrakenRestMethod -Exactly -Times 1 -ParameterFilter {
            $Uri -eq $url -and $Method -eq "Get"
        }    
    }

    It "Passthrough info and pair" {
        Assert-MockCalled Invoke-PSKrakenRestMethod -ParameterFilter {
            $Body["info"] -eq "leverage" -and $Body["pair"] -eq "XETHZEUR"            
        }
    }

    It "Return response result" {
        $result | Should Be "success"
    }    
}

Describe "Get-TickerInformation" {
    $url = "https://api.kraken.com/0/public/Ticker"

    Mock Invoke-PSKrakenRestMethod { return "success" }

    $result = Get-PSKrakenTickerInformation -Pair XETHZEUR, XBTZEUR

    It "Request $url" {
        Assert-MockCalled Invoke-PSKrakenRestMethod -Exactly -Times 1 -ParameterFilter {
            $Uri -eq $url -and $Method -eq "Get"            
        }
    }

    It "Passthrough pair arugment" {
        Assert-MockCalled Invoke-PSKrakenRestMethod -ParameterFilter {
            $Body["pair"] -eq "XETHZEUR,XBTZEUR"
        }
    }

    It "Return response result" {
        $result | Should Be "Success"
    }
}

Describe "Get-PSKrakenOHLCData" {
    $url = "https://api.kraken.com/0/public/OHLC"

    Mock Invoke-PSKrakenRestMethod { return "success" }

    $result = Get-PSKrakenOHLCData -Pair XBTZEUR -Interval 5 -Since "some_id_1"

    It "Request $url" {
        Assert-MockCalled Invoke-PSKrakenRestMethod -Exactly -Times 1 -ParameterFilter {
            $Uri -eq $url -and $Method -eq "Get"
        }
    }

    It "Passthrough pair, interval, since" {
        Assert-MockCalled Invoke-PSKrakenRestMethod -ParameterFilter {
            $Body["pair"] -eq "XBTZEUR" `
                -and $Body["interval"] -eq 5 `
                -and $Body["since"] -eq "some_id_1"
        }
    }

    It "Return response result" {
        $result | Should Be "success"
    }
}

Describe "Get-PSKrakenOrderBook" {
    $url = "https://api.kraken.com/0/public/Depth"

    Context "Mocked response" {

    }

    Context "Validate response JSON schema" {
        $responseData = Invoke-PSKrakenRestMethod -Method Get -Uri "https://api.kraken.com/0/public/Depth" -Body @{pair="XETHZEUR";count="10"}
        Assert-JsonSchema -JsonString ($responseData | ConvertTo-Json -Depth 100) -JsonSchema (Get-Content -Raw .\ResponseGetOrderBookSchema.json)
    }
}

Describe "Get-PSKrakenRecentTrades" {
    $url = "https://api.kraken.com/0/public/Trades"

    Mock Invoke-PSKrakenRestMethod { return "success" }

    $result = Get-PSKrakenRecentTrades -Pair "XETHZEUR" -Since "123"

    It "Request $url" {
        Assert-MockCalled Invoke-PSKrakenRestMethod -Exactly -Times 1 -ParameterFilter {
            $Uri -eq $url
        }
    }

    It "Passthrough pair and since" {
        Assert-MockCalled Invoke-PSKrakenRestMethod -ParameterFilter {
            $Body["pair"] -eq "XETHZEUR" -and $Body["since"] -eq "123"
        }
    }

    It "Return response result" {
        $result | Should Be "success"
    }
}

Describe "Get-PSKrakenRecentSpreadData" { 
    $url = "https://api.kraken.com/0/public/Spread"

    Mock Invoke-PSKrakenRestMethod { return "success" }

    $result = Get-PSKrakenRecentSpreadData -Pair "XETHZEUR" -Since "123"
    
    It "Request $url" {
        Assert-MockCalled Invoke-PSKrakenRestMethod -Exactly -Times 1 -ParameterFilter {
            $Uri -eq $url
        }
    }

    It "Passthrough pair and since" {
        Assert-MockCalled Invoke-PSKrakenRestMethod -ParameterFilter {
            $Body["pair"] -eq "XETHZEUR" -and $Body["since"] -eq "123"
        }
    }

    It "Return response result" {
        $result | Should Be "success"
    }
}   

