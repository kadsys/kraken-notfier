$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path) -replace '\.Tests\.', '.'
. "$here\$sut"

Describe "ConvertTo-PSKrakenDateTime" {
    $result = ConvertTo-PSKrakenDateTime -UnixTimestamp 1495961042

    It "Result is a DateTime object" {
        $result | Should BeOfType [DateTime]
    }

    It "Convert to 'Sunday, 28-May-17 08:44:02'" {
        $result.Month | Should Be 5
        $result.Day | Should Be 28
        $result.Year | Should Be 2017
        $result.Hour | Should Be 8
        $result.Minute | Should Be 44
        $result.Second | Should Be 2
    }
}

Describe "Get-PSKrakenTrades" {
    $trades = @(@("157.49999", "1.61315991", 1495966550.291, "b", "l", ""), @("157.49998", "3.26697270", 1495966550.3059, "s", "m", ""))
    $fakeResult = New-Object -TypeName PSObject -Property @{
        XETHZEUR = $trades;
        last     = "1495969415728666607"
    }

    Mock Get-PSKrakenRecentTrades { return $fakeResult }
    
    # $t = $result.XETHZEUR | Select-Object -First 2 | ForEach-Object {
    #     New-Object -TypeName PSObject -Property @{
    #         Date = (ConvertTo-PSKrakenDateTime -UnixTimestamp $_[2])
    #     }
    # }

    $result = Get-PSKrakenTrades -Pair XETHZEUR -Since "123"

    It "Invokes Get-PSKrakenRecentTrades to get the recent trades" {
        Assert-MockCalled Get-PSKrakenRecentTrades -Exactly -Times 1 -ParameterFilter {
            $Pair -eq "XETHZEUR" -and $Since -eq "123"
        }
    }

    It "Contains custom object with buys, sells and last id which can be used for polling the data" {
        $result | Should BeOfType [PSObject]
        $result.Buys | Measure-Object | Select-Object -ExpandProperty Count | Should Be 1
        $result.Sells | Measure-Object | Select-Object -ExpandProperty Count | Should Be 1
        $result.Last | Should Be "1495969415728666607"

        $firstBuy = $result.Buys | Select-Object -First 1
        $firstBuy.Price | Should Be 157.49999
        $firstBuy.Volume | Should Be 1.61315991
        $firstBuy.Date | Should Be (ConvertTo-PSKrakenDateTime -UnixTimestamp 1495966550.291)
        $firstBuy.OrderType | Should Be limit

        $firstSell = $result.Sells | Select-Object -First 1
        $firstSell.Price | Should Be 157.49998
        $firstSell.Volume | Should Be 3.26697270
        $firstSell.Date | Should Be (ConvertTo-PSKrakenDateTime -UnixTimestamp 1495966550.3059)
        $firstSell.OrderType | Should Be market
    }
}