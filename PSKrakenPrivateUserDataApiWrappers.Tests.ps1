Describe "Get-PSKrakenRecentSpreadData" {
    $url = "https://api.kraken.com/0/public/Spread"

    It "Request $url" {

    }

    It "Passthrough pair" {

    }

    It "Passthrough since" {

    }

    It "Return response result" {

    }
}

Describe "Get-AccountBalance" {
    $url = "https://api.kraken.com/0/private/Balance"

    It "Request $url" {

    }

    It "Return response result" {

    }
}

Describe "Get-TradeBalance" {
    $url = "https://api.kraken.com/0/private/TradeBalance"

    It "Request $url" {

    }

    It "Passthrough asset" {

    }

    It "Return response result" {

    }
}

Describe "Get-OpenOrders" {
    $url = "https://api.kraken.com/0/private/OpenOrders"

    It "Request $url" {

    }

    It "Passthrough trades" {

    }

    It "Passthrough userref" {

    }

    It "Return response result" {

    }
}

Describe "Get-ClosedOrders" {
    $url = "https://api.kraken.com/0/private/ClosedOrders"

    It "Request $url" {

    }

    It "Passthrough trades" {

    }

    It "Passthrough userref" {

    }

    It "Passthrough start" {

    }

    It "Passthrough end" {

    }

    It "Passthrough ofs" {

    }

    It "Passthrough Closetime" {

    }

    It "Return response result" {

    }
}

Describe "Get-QueryOrders" {
    $url = "https://api.kraken.com/0/private/QueryOrders"

    It "Request $url" {

    }

    It "Passthrough trades" {

    }

    It "Passthrough userref" {

    }

    It "Passthrough txid" {

    }

    It "Return response result" {

    }
}

Describe Get-TradesHistory {
    $url = "https://api.kraken.com/0/private/TradesHistory"

    It "Request $url" {

    }

    It "Passthrough type" {

    }

    It "Passthrough trades" {

    }

    It "Passthrough start" {

    }

    It "Passthrough end" {

    }

    It "Passthrough ofs" {

    }

    It "Return response result" {

    }
}

Describe "Get-QueryTrades" {
    $url = "https://api.kraken.com/0/private/QueryTrades"

    It "Request $url" {

    }

    It "Passthrough txid" {

    }

    It "Passthrough trades" {

    }

    It "Return response result" {

    }
}

Describe "Get-OpenPositions" {
    $url = "https://api.kraken.com/0/private/OpenPositions"

    It "Request $url" {

    }

    It "Passthrough txid" {

    }

    It "Passthrough docalcs" {

    }

    It "Return response result" {

    }
}


Describe "Get-Ledgers" {
    $url = "https://api.kraken.com/0/private/Ledgers"

    It "Request $url" {

    }

    It "Passthrough aclass" {

    }

    It "Passthrough asset" {

    }

    It "Passthrough type" {

    }

    It "Passthrough start" {

    }

    It "Passthrough end" {

    }

    It "Passthrough ofs" {

    }

    It "Return response result" {

    }
}

Describe "Get-QueryLedgers" {
    $url = "https://api.kraken.com/0/private/QueryLedgers"

    It "Request $url" {

    }

    It "Passthrough id" {

    }

    It "Return response result" {

    }
}

Describe "Get-TradeVolume" {
    $url = "https://api.kraken.com/0/private/TradeVolume"

    It "Request $url" {

    }

    It "Passthrough pair" {

    }

    It "Passthrough fee-info" {

    }    

    It "Return response result" {

    }
}

Describe "Add-Order" {
    $url = "https://api.kraken.com/0/private/AddOrder"

    It "Request $url" {

    }

    It "Passthrough pair" {

    }

    It "Passthrough type" {

    }

    It "Passthrough ordertype" {

    }

    It "Passthrough price" {

    }

    It "Passthrough price2" {

    }

    It "Passthrough volume" {

    }

    It "Passthrough leverage" {

    }

    It "Passthrough oflags" {

    }

    It "Passthrough starttm" {

    }

    It "Passthrough expiretm" {

    }

    It "Passthrough userref" {

    }

    It "Passthrough validate" {

    }

    It "Passthrough close[ordertype]" {

    }
    
    It "Passthrough close[price]" {
        
    }

    It "Passthrough close[price2]" {
        
    }    

    It "Return response result" {

    }
}

Describe "Close-Order" {
    $url = "https://api.kraken.com/0/private/CancelOrder"

    It "Request $url" {

    }

    It "Passthrough txid" {

    }

    It "Return response result" {

    }
}

Describe "Get-DepositMethods" {
    $url = "https://api.kraken.com/0/private/DepositMethods"

    It "Request $url" {

    }

    It "Passthrough aclass" {

    }

    It "Passthrough asset" {

    }

    It "Return response result" {

    }    
}

Describe "Get-DepositAddresses" {
    $url = "https://api.kraken.com/0/private/DepositAddresses"

    It "Request $url" {

    }

    It "Passthrough aclass" {

    }

    It "Passthrough asset" {

    }

    It "Passthrough method" {

    }

    It "Passthrough new" {

    }

    It "Return response result" {

    }
}

Describe "Get-DepositStatus" {
    $url = "https://api.kraken.com/0/private/DepositStatus"

    It "Request $url" {

    }

    It "Passthrough aclass" {

    }

    It "Passthrough asset" {

    }

    It "Passthrough method" {

    }

    It "Return response result" {

    }
}

Describe "Get-WithdrawInfo" {
    $url = "https://api.kraken.com/0/private/WithdrawInfo"

    It "Request $url" {

    }

    It "Passthrough aclass" {

    }

    It "Passthrough asset" {
        
    }

    It "Passthrough key" {
        
    }    

    It "Passthrough amount" {
        
    }    

    It "Return response result" {

    }
}

Describe "New-Withdraw" {
    $url = "https://api.kraken.com/0/private/Withdraw"

    It "Request $url" {

    }

    It "Passthrough aclass" {

    }

    It "Passthrough asset" {
        
    }

    It "Passthrough key" {
        
    }    

    It "Passthrough amount" {
        
    }    

    It "Return response result" {
        
    }
}

Describe "Get-WithdrawStatus" {
    $url = "https://api.kraken.com/0/private/WithdrawStatus"

    It "Request $url" {

    }

    It "Passthrough aclass" {

    }

    It "Passthrough asset" {
        
    }

    It "Passthrough method" {
        
    }    

    It "Return response result" {
        
    }
}

Describe "Get-WithdrawStatus" {
    $url = "https://api.kraken.com/0/private/WithdrawStatus"

    It "Request $url" {

    }

    It "Passthrough aclass" {

    }

    It "Passthrough asset" {
        
    }

    It "Passthrough method" {
        
    }    

    It "Return response result" {
        
    }
}

Describe "Close-Withdraw" {
    $url = "https://api.kraken.com/0/private/WithdrawCancel"

    It "Request $url" {

    }

    It "Passthrough aclass" {

    }

    It "Passthrough asset" {
        
    }

    It "Passthrough refid" {
        
    }    

    It "Return response result" {
        
    }    
}