$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path) -replace '\.Tests\.', '.'
. "$here\$sut"

# $url = "https://api.kraken.com/0/public/Time"
# $jsonSchema = Invoke-RestMethod -Uri $url -Method Get | 
#     Select-Object -ExpandProperty result | 
#     ConvertTo-Json |
#     Get-JsonSchema

# Assert-JsonSchema -JsonString '{"unixtime":"1496442828","rfc1123":123}' -JsonSchema $jsonSchema

Describe 'Assert-JsonSchema' {
    Context 'Output format of the remote service to validate the JSON content changed' {
        Mock Invoke-RestMethod { return ("{`"message`":`"success`"}" | ConvertFrom-Json)}

        It "Throw an exception that the output format (output JSON Schema) has been changed" { 
            { 
                Assert-JsonSchema -JsonSchema "no matter" -JsonString "no matter" 
            } | Should Throw "Response output format (JSON Schema) has been changed"
        }
    }

    Context 'Requester throws an exception' {
        Mock Invoke-RestMethod { throw "Bad Request" }

        It 'Rethrow the exception' {
            { Assert-JsonSchema -JsonSchema "no matter" -JsonString "no matter" } | Should Throw "Bad Request"
        }
    }     

    Context 'Invalid JSON Schema (non JSON format)' {
        Mock Invoke-RestMethod { return ("{`"input-invalid`":{`"message`":`"JSON Schema invalid`"}}" | ConvertFrom-Json) }

        It 'Throw an exception that the input JSON Schema has an invalid JSON format' {
            { 
                Assert-JsonSchema  -JsonSchema "no matter" -JsonString "no matter" } | Should Throw "Input JSON Schema invalid"
        }
    }

    Context 'Invalid asserted JSON string (non JSON format)' {
        Mock Invoke-RestMethod { return ("{`"input2-invalid`":{`"message`":`"JSON content invalid`"}}" | ConvertFrom-Json) }

        It 'Throw an exception that the input JSON content has an invalid JSON format' {
            { Assert-JsonSchema  -JsonSchema "no matter" -JsonString "no matter" } | Should Throw "Input JSON data invalid"
        }
    }

    Context 'Invalid JSON Schema and asserted JSON string (non JSON format)' {
        Mock Invoke-RestMethod { return ("{`"input-invalid`":{`"message`":`"JSON Schema invalid`"},`"input2-invalid`":{`"message`":`"JSON content invalid`"}}" | ConvertFrom-Json) }

        It 'Throw an exception that the input JSON Schema and assert content have an invalid JSON format' {
            { Assert-JsonSchema  -JsonSchema "no matter" -JsonString "no matter" } | Should Throw "Input JSON Schema invalid"
        }
    }

    Context 'Input JSON content does not satisfy the given JSON Schema' {
        $failedResponse = Get-Content .\FailedJsonValidationResponse.json | ConvertFrom-Json
        $expectedFailures = $failedResponse | Select-Object -ExpandProperty results | ConvertFrom-Json

        Mock Invoke-RestMethod { return  $failedResponse }

        It 'Throw an exception an write the errors' {
            { 
                Assert-JsonSchema -JsonSchema "no matter" -JsonString "no matter"
            } | Should Throw "JSON data do not satisfy the JSON Schema"
        }
    }

    Context 'Input JSON content is correct' {
        Mock Invoke-RestMethod { return ('{"valid":true,"results":"[ ]"}' | ConvertFrom-Json) }

        It 'No result should be present' {
            Assert-JsonSchema -JsonSchema "no matter" -JsonString "no matter" | Should BeNullOrEmpty
        }
    }
}