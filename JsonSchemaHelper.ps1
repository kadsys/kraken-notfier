function Get-JsonSchema {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory, ValueFromPipeline)]
        [String]$JsonString
    )

    $ErrorActionPreference = [System.Management.Automation.ActionPreference]::Stop
    
    $headers = @{
        "User-Agent"      = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
        "Content-Type"    = "application/json";
        "Referer"         = "https://jsonschema.net";
        "Accept-Encoding" = "gzip, deflate, br"
    }
    
    $options = New-Object -TypeName PSObject -Property @{
        "rootId"      = "http://example.com/example.json";
        "json"        = $JsonString;
        "commonState" = $true;
        "verbose"     = $false;
        "metadata"    = $false;
        "enum"        = $false;
        "idType"      = "RELATIVE";
        "objectState" = $false;
        "arrayState"  = $false;
        "items"       = "LIST";
        "stringState" = $false;
        "numberState" = $false
    }

    $body = New-Object -TypeName psobject -Property @{
        "options" = $options
    } | ConvertTo-Json

    $response = Invoke-RestMethod -Uri "https://json-schema.appspot.com/_ah/api/jschema/v1/schemify?alt=json&key=AIzaSyCMJ6G425OG4N-2t3VZLAo3mPPM54h88RY" `
        -Method Post -Headers $headers -Body $body

    $response.schema | ConvertFrom-Json | ConvertTo-Json -Depth 100
}

function Assert-JsonSchema {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory, ValueFromPipeline)]
        [String]$JsonString,

        [Parameter(Mandatory)]
        [String]$JsonSchema
    )
   
    $ErrorActionPreference = [System.Management.Automation.ActionPreference]::Stop
    
    $headers = @{
        "User-Agent"      = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
        "Content-Type"    = "application/x-www-form-urlencoded; charset=UTF-8";
        "Accept-Encoding" = "gzip, deflate, br"
    }

    Add-Type -AssemblyName System.Web

    $body = "input=$([System.Web.HttpUtility]::UrlEncode($JsonSchema))&input2=$([System.Web.HttpUtility]::UrlEncode($JsonString))"

    $response = Invoke-RestMethod -Uri "https://json-schema-validator.herokuapp.com/process/index" `
        -Method Post -Headers $headers -Body $body

    $ErrorActionPreference = [System.Management.Automation.ActionPreference]::Continue

    if ($response.valid -eq $null -or $response.results -eq $null) {
        if ($response."input-invalid" -ne $null) {
            throw "Input JSON Schema invalid"
        }  elseif ($response."input2-invalid" -ne $null) {
            throw "Input JSON data invalid"
        } else {
            throw "Response output format (JSON Schema) has been changed"            
        }
    }
    
    if (!$response.valid) {
        throw "JSON data do not satisfy the JSON Schema"
    }
}