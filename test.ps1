# New-BurntToastNotification -Text "Start" -Silent -UniqueIdentifier $toastId

# $counter = 0
# while ($true) {
#     $counter += 1
#     New-BurntToastNotification -Text "Counter: $counter" -Silent -UniqueIdentifier $toastId 
#     Start-Sleep -Seconds 10 
# }

# Add-Type -TypeDefinition @"
#     public enum Asset
#     {
#         ETH,
#         BTC
#     }
# "@

# function New-TypeFromArray {
#     param(
#         [Parameter(Mandatory)]
#         [Array]$EnumeratorList                
#     )

#     $typeDefinition = [String]::Join(",", $EnumeratorList)

#     Add-Type -TypeDefinition @"
#         public enum AssetEnum
#         {  
#             $typeDefinition    
#         }
# "@
# }

# Describe "New-TypeFromArray" {
#     $result = New-TypeFromArray ETH,BTC,DASH

#     It "Be an enum type" {
#         $result | Should BeOfType [AssetEnum]
#     }
# }