enum AssetInfo {
    info
    leverage
    fees
    margin
}

enum Order {
    buy
    sell
}

enum OrderType {
    market
    limit
}