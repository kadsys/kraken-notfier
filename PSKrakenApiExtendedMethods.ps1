. .\PSKrakenPublicMarketDataApiWrappers.ps1

function Get-PSKrakenTrades {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [String]$Pair,

        [String]$Since
    )

    $result = Get-PSKrakenRecentTrades -Pair $Pair -Since $Since

    $convert = {
        New-Object -TypeName PSObject -Property @{
            Price     = [double]$_[0];
            Volume    = [double]$_[1];
            Date      = (ConvertTo-PSKrakenDateTime -UnixTimestamp $_[2]);
            Order     = (& {if ($_[3] -eq "b") { [Order]::buy } else { [Order]::sell }})
            OrderType = (& {if ($_[4] -eq "l") { [OrderType]::limit } else { [OrderType]::market }})
        }    
    }

    $trades = $result.$Pair | ForEach-Object $convert
    $buys = $trades | Where-Object Order -eq buy
    $sells = $trades | Where-Object Order -eq sell

    $result = New-Object -TypeName PSObject -Property @{
        Buys  = $buys;
        Sells = $sells;
        Last  = $result.last
    }

    return $result
}