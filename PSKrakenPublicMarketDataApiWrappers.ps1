. .\PSKrakenRestMethods.ps1
. .\PSKrakenApiEnumTypes.ps1

function Get-PSKrakenServerTime {
    [CmdletBinding()]
    param()

    Invoke-PSKrakenRestMethod -Method Get -Uri "https://api.kraken.com/0/public/Time"    
}

function Get-PSKrakenAssets {
    [CmdletBinding()]
    param(
        [string]$Asset
    )
    $ErrorActionPreference = [System.Management.Automation.ActionPreference]::Stop
 
    $body = @{}

    if ($Asset) {
        $body["asset"] = $Asset
    }

    $assets = Invoke-PSKrakenRestMethod -Method Get -Uri "https://api.kraken.com/0/public/Assets" -Body $body

    $result = $assets.PsObject.Properties | 
        Select-Object -ExpandProperty Name | 
        ForEach-Object {
        New-Object -TypeName psobject -Property @{
            Name            = $_;
            AClass          = $assets.$_.aclass;
            Altname         = $assets.$_.altname;
            Decimals        = $assets.$_.decimals;
            DisplayDecimals = $assets.$_.display_decimals;
        }
    }

    return $result
}

function Get-PSKrakenTradableAssetPairs {
    [CmdletBinding()]
    param(
        [AssetInfo]$Info,
        [string]$Pair        
    )

    $body = @{}
    if ($Info -ne $null) {
        $body["info"] = $Info
    }

    if ($Pair) {
        $body["pair"] = $Pair
    }

    Invoke-PSKrakenRestMethod -Method Get -Uri "https://api.kraken.com/0/public/AssetPairs" -Body $body
}

function Get-PSKrakenTickerInformation {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [Array]$Pair        
    )

    $body = @{
        pair = [string]::Join(",", $Pair)
    }

    Invoke-PSKrakenRestMethod -Method Get -Uri "https://api.kraken.com/0/public/Ticker" -Body $body
}

function Get-PSKrakenOHLCData {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [string]$Pair,

        # time interval in minutes (default 1)
        [int]$Interval,

        [string]$Since        
    )

    $body = @{
        pair = $Pair
    }

    if ($Interval) {
        $body["interval"] = $Interval
    }

    if ($Since) {
        $body["since"] = $Since
    }

    Invoke-PSKrakenRestMethod -Method Get -Uri "https://api.kraken.com/0/public/OHLC" -Body $body
}

function ConvertTo-PSKrakenDateTime {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [double]$UnixTimestamp
    )

    $dateTime = [DateTime]::new(1970, 1, 1)
    
    return $dateTime.AddSeconds($UnixTimestamp)
}

function Get-PSKrakenOrderBook {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [string]$Pair,

        [int]$Count
    )

    $body = @{
        pair = $Pair
    }

    if ($Count) {
        $body["count"] = $Count
    }

    $response = Invoke-PSKrakenRestMethod -Method Get -Uri "https://api.kraken.com/0/public/Depth" -Body $body

    $convert = {
        New-Object -TypeName PSObject -Property @{
            Price  = [double]$_[0]; 
            Volume = [double]$_[1]; 
            Date   = (ConvertTo-PSKrakenDateTime -UnixTimestamp $_[2])
        }
    }

    return New-Object -TypeName PSObject -Property @{
        Asks = $response.asks | ForEach-Object $convert;
        Bids = $response.bids | ForEach-Object $convert
    }    
}

function Get-PSKrakenRecentTrades {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [string]$Pair,

        [string]$Since
    )

    $body = @{
        pair = $Pair
    }

    if ($Since) {
        $body["since"] = $Since
    }

    Invoke-PSKrakenRestMethod -Method Get -Uri "https://api.kraken.com/0/public/Trades" -Body $body
}

function Get-PSKrakenRecentSpreadData {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [string]$Pair,

        [string]$Since
    )

    $body = @{
        pair = $Pair
    }

    if ($Since) {
        $body["since"] = $Since
    }

    Invoke-PSKrakenRestMethod -Method Get -Uri "https://api.kraken.com/0/public/Spread" -Body $body
} 