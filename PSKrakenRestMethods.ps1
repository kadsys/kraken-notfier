function Invoke-PSKrakenRestMethod {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [Microsoft.PowerShell.Commands.WebRequestMethod]$Method,

        [Parameter(Mandatory)]
        [Uri]$Uri,
        
        [Object]$Body
    )

    $ErrorActionPreference = [System.Management.Automation.ActionPreference]::Stop

    $response = Invoke-RestMethod -Method $Method -Uri $Uri -Body $Body

    if ($response.error) {
        throw $response.error
    }

    return $response.result
}